package com.developer.aitek.earthquake.utilities

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.google.gson.Gson

class CacheUtil {
    private var sharePref: SharedPreferences? = null

    fun start(activity: Activity, name: String) {
        this.sharePref = activity.getSharedPreferences(name, Context.MODE_PRIVATE)
    }

    fun destroy() { this.sharePref = null }

    fun <T> set(name: String, value: T) {
        this.sharePref?.let {
            with(it.edit()) {
                putString(name, Gson().toJson(value))
                Log.d("CACHE UTIL", name)
                apply()
            }
        }
    }

    fun clear() {
        this.sharePref?.edit()?.clear()?.apply()
    }

    fun get(name: String): String? {
        if (sharePref != null) return sharePref!!.getString(name, null)
        return null
    }
}
package com.developer.aitek.earthquake.activities.fragments

import android.app.Activity
import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.developer.aitek.earthquake.R
import com.developer.aitek.earthquake.activities.DataDeviceActivity
import com.developer.aitek.earthquake.models.Device
import com.developer.aitek.earthquake.models.getDevices
import com.developer.aitek.earthquake.models.switchAlarm
import com.developer.aitek.earthquake.utilities.*
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.component_device.*
import kotlinx.android.synthetic.main.item_device.view.*
import java.util.*

class DeviceFragment : Fragment() {
    private lateinit var rootActivity: Activity
    private var bundle: Bundle? = null
    private lateinit var deviceAdapter: AdapterUtil<Device.Data>
    private lateinit var cacheUtil: CacheUtil

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(rootActivity, getString(R.string.package_name))

        // Setup Device Adapter
        deviceAdapter = AdapterUtil(R.layout.item_device, listOf(),
            { itemView, item ->
                itemView.item_device_name.text = item.device_name
                if (item.map != null) {
                    val address = Geocoder(requireContext(), Locale.getDefault())
                        .getFromLocation(item.map!!.lat, item.map!!.lon, 1)[0]
                    val addressLine = address.getAddressLine(0).split(",")
                    itemView.item_device_location.text = addressLine[0]
                } else {
                    itemView.item_device_location.text = "-"
                }

                itemView.switchAlarmBtn.text = item.alarm_status
                itemView.switchAlarmBtn.setOnClickListener {
                    updateAlarm(if (item.alarm_status == "ON") "OFF" else "ON", item.id)
                }

                itemView.item_device_location.setOnClickListener {
                    selected_device = item
                    startActivity(Intent(requireContext(), DataDeviceActivity::class.java))
                    requireActivity().finish()
                }
                itemView.item_device_name.setOnClickListener {
                    selected_device = item
                    startActivity(Intent(requireContext(), DataDeviceActivity::class.java))
                    requireActivity().finish()
                }
            },
            { _, _ ->
            })

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_device, container, false)
    }

    private fun updateAlarm(status: String, id: Long) {
        loader_device.visibility = View.VISIBLE
        list_device.visibility = View.GONE
        val data = JsonObject()
        data.addProperty("status", status)
        switchAlarm(auth(this.cacheUtil).token, id, data, {
            if (it.code == 200) {
                Toast.makeText(requireContext(), "Berhasil mengubah status alarm device", Toast.LENGTH_LONG).show()
                loadDevice()
            } else {
                Toast.makeText(requireContext(), "Gagal mengubah status alarm device", Toast.LENGTH_LONG).show()
            }
            loader_device.visibility = View.GONE
            list_device.visibility = View.VISIBLE
        }, {
            Toast.makeText(requireContext(), "Gagal mengubah status alarm device", Toast.LENGTH_LONG).show()
            loader_device.visibility = View.GONE
            list_device.visibility = View.VISIBLE
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Setup Device
        list_device.layoutManager = LinearLayoutManager(requireContext())
        list_device.adapter = deviceAdapter

        // Load Device
        this.loadDevice()
    }

    private fun loadDevice() {
        loader_device.visibility = View.VISIBLE
        list_device.visibility = View.GONE
        getDevices(auth(this.cacheUtil).token, {
            if (it.code == 200) {
                deviceAdapter.data = it.data
            } else Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()

            loader_device.visibility = View.GONE
            list_device.visibility = View.VISIBLE
        }, {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            loader_device.visibility = View.GONE
            list_device.visibility = View.VISIBLE
        })
    }

    companion object {
        @JvmStatic
        fun newInstance(activity: Activity, bundle: Bundle?) =
            DeviceFragment().apply {
                this.rootActivity = activity
                this.bundle = bundle
            }
    }
}
package com.developer.aitek.earthquake

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.developer.aitek.earthquake.activities.CheckIsAlreadyAuthActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startActivity(Intent(this@MainActivity, CheckIsAlreadyAuthActivity::class.java))
        finish()
    }
}
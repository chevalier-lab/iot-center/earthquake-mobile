package com.developer.aitek.earthquake.models

// Weather Item
data class ItemWeather(
    var icon: Int,
    var value: String,
    var description: String
)
package com.developer.aitek.earthquake.utilities

import java.util.*

interface PermissionResultCallback {
    fun PermissionGranted(request_code: Int)
    fun PartialPermissionGranted(
        request_code: Int,
        granted_permissions: ArrayList<String>
    )

    fun PermissionDenied(request_code: Int)
    fun NeverAskAgain(request_code: Int)
}
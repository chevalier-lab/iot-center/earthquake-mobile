package com.developer.aitek.earthquake.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.text.TextUtils
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.developer.aitek.earthquake.R
import com.developer.aitek.earthquake.models.doRegistration
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_registration.*

class RegistrationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        // Render HTML Code
        tv_sign_in.text = Html.fromHtml(getString(R.string.message_sign_up))

        // Show Hide Password
        var isShow = false
        edt_password.setOnTouchListener { view, event ->
            val DRAWABLE_RIGHT = 2;

            if(event.action == MotionEvent.ACTION_UP) {
                if(event.rawX >=
                    (edt_password.right - edt_password.compoundDrawables[DRAWABLE_RIGHT].bounds.width())) {
                    // your action here
                    if (isShow) {
                        isShow = false
                        edt_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_close, 0)
                        edt_password.transformationMethod = PasswordTransformationMethod.getInstance()
                    } else {
                        isShow = true
                        edt_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_open, 0)
                        edt_password.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    }

                    return@setOnTouchListener true
                }
            }
            return@setOnTouchListener false
        }

        // Handler Inputs
        setActivationButton()
        edt_username.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                setActivationButton()
            }
        })
        edt_password.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                setActivationButton()
            }
        })
        edt_phone_number.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                setActivationButton()
            }
        })
        edt_full_name.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                setActivationButton()
            }
        })

        // Handler Sign In
        btn_sign_up.setOnClickListener {
            loader.visibility = View.VISIBLE
            btn_sign_up.visibility = View.GONE
            val data = JsonObject()
            data.addProperty("full_name", edt_full_name.text.toString())
            data.addProperty("username", edt_username.text.toString())
            data.addProperty("password", edt_password.text.toString())
            data.addProperty("phone_number", "62${edt_password.text}")
            data.addProperty("level", "1")
            doRegistration(data, {
                loader.visibility = View.GONE
                btn_sign_up.visibility = View.VISIBLE
                if (it.code == 200) {
                    Toast.makeText(this@RegistrationActivity, "Berhasil melakukan registrasi, harap login",
                        Toast.LENGTH_LONG).show()
                    startActivity(Intent(this@RegistrationActivity, LoginActivity::class.java))
                    finish()
                } else Toast.makeText(this@RegistrationActivity, it.message, Toast.LENGTH_LONG).show()
            }, {
                Toast.makeText(this@RegistrationActivity, it, Toast.LENGTH_LONG).show()
                loader.visibility = View.GONE
                btn_sign_up.visibility = View.VISIBLE
            })
        }

        // Handler Sign Up
        tv_sign_in.setOnClickListener {
            startActivity(Intent(this@RegistrationActivity, LoginActivity::class.java))
            finish()
        }
    }

    private fun setActivationButton() {
        if (checkIsFilled()) {
            btn_sign_up.isEnabled = true
            btn_sign_up.setBackgroundResource(R.drawable.background_round_orange)
            btn_sign_up.setTextColor(
                ContextCompat.getColor(applicationContext,
                    R.color.colorWhite))
        } else {
            btn_sign_up.isEnabled = false
            btn_sign_up.setBackgroundResource(R.drawable.background_round_dark_gray)
            btn_sign_up.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorBlack))
        }
    }

    private fun checkIsFilled(): Boolean {
        return !(TextUtils.isEmpty(edt_username.text.toString()) ||
                TextUtils.isEmpty(edt_password.text.toString()) ||
                TextUtils.isEmpty(edt_phone_number.text.toString()) ||
                TextUtils.isEmpty(edt_full_name.text.toString()))
    }
}
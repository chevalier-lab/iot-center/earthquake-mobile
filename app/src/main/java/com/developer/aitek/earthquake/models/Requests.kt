package com.developer.aitek.earthquake.models

import com.developer.aitek.earthquake.apis.ApiClient
import com.developer.aitek.earthquake.utilities.log
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// Login
fun doLogin(
    data: JsonObject,
    onSuccess: (Login) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<Login> = ApiClient.getClient
        .login(data)
    call.enqueue(object : Callback<Login> {
        override fun onFailure(call: Call<Login>, t: Throwable) {
            log(t.message.toString())
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<Login>, response: Response<Login>) {
            log(response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Registration
fun doRegistration(
    data: JsonObject,
    onSuccess: (Login) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<Login> = ApiClient.getClient
        .registration(data)
    call.enqueue(object : Callback<Login> {
        override fun onFailure(call: Call<Login>, t: Throwable) {
            log(t.message.toString())
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<Login>, response: Response<Login>) {
            log(response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Profile
fun getProfile(
    api_key: String,
    onSuccess: (Profile) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<Profile> = ApiClient.createService(api_key)
        .profile()
    call.enqueue(object : Callback<Profile> {
        override fun onFailure(call: Call<Profile>, t: Throwable) {
            log(t.message.toString())
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<Profile>, response: Response<Profile>) {
            log(response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Data Device
fun getDataDevice(
    api_key: String,
    onSuccess: (DataDevice) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<DataDevice> = ApiClient.createService(api_key)
        .dataDevice()
    call.enqueue(object : Callback<DataDevice> {
        override fun onFailure(call: Call<DataDevice>, t: Throwable) {
            log(t.message.toString())
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<DataDevice>, response: Response<DataDevice>) {
            log(response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Weather
fun getWeather(
    lat: Double,
    lon: Double,
    api_key: String,
    onSuccess: (Weather) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<Weather> = ApiClient.getClientWeather
        .getWeather(lat, lon, api_key)
    call.enqueue(object : Callback<Weather> {
        override fun onFailure(call: Call<Weather>, t: Throwable) {
            log(t.message.toString())
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<Weather>, response: Response<Weather>) {
            log(response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// BMKG
fun getBMKG(
    date_from: String,
    date_to: String,
    token: String,
    onSuccess: (BMKG) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<BMKG> = ApiClient.createService(token)
        .getThisMonthBMKG(date_from, date_to)
    call.enqueue(object : Callback<BMKG> {
        override fun onFailure(call: Call<BMKG>, t: Throwable) {
            log(t.message.toString())
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<BMKG>, response: Response<BMKG>) {
            log(response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Get Devices
fun getDevices(
    token: String,
    onSuccess: (Device) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<Device> = ApiClient.createService(token)
        .getDevices()
    call.enqueue(object : Callback<Device> {
        override fun onFailure(call: Call<Device>, t: Throwable) {
            log(t.message.toString())
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<Device>, response: Response<Device>) {
            log(response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Get Notifications
fun getNotifications(
    token: String,
    onSuccess: (Notification) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<Notification> = ApiClient.createService(token)
        .getNotification()
    call.enqueue(object : Callback<Notification> {
        override fun onFailure(call: Call<Notification>, t: Throwable) {
            log(t.message.toString())
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<Notification>, response: Response<Notification>) {
            log(response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// get data devices
fun getLastDataDevices(
        token: String,
        onSuccess: (LastDeviceData) -> Unit,
        onFailed: (String) -> Unit
) {
    val call: Call<LastDeviceData> = ApiClient.createService(token).getLastDataDevice()
    call.enqueue(object : Callback<LastDeviceData> {
        override fun onFailure(call: Call<LastDeviceData>, t: Throwable) {
            log(t.message.toString())
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<LastDeviceData>, response: Response<LastDeviceData>) {
            log(response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}

// Switch Alarm Device
fun switchAlarm(
    token: String,
    device_id: Long,
    data: JsonObject,
    onSuccess: (SwitchAlarmDevice) -> Unit,
    onFailed: (String) -> Unit
) {
    val call: Call<SwitchAlarmDevice> = ApiClient.createService(token).switchAlarmDevice(device_id, data)
    call.enqueue(object : Callback<SwitchAlarmDevice> {
        override fun onFailure(call: Call<SwitchAlarmDevice>, t: Throwable) {
            log(t.message.toString())
            onFailed(t.message.toString())
        }

        override fun onResponse(call: Call<SwitchAlarmDevice>, response: Response<SwitchAlarmDevice>) {
            log(response.body().toString())
            if (response.isSuccessful)
                if (response.body() != null) onSuccess(response.body()!!)
                else onFailed(response.message().toString())
            else onFailed(response.message().toString())
        }
    })
}
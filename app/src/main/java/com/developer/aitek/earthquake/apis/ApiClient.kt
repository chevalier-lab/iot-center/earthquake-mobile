package com.developer.aitek.earthquake.apis

import android.text.TextUtils
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {

    var BASE_URL: String = "http://213.190.4.40/earthquake-back-end/index.php/api/"
    var BASE_URL_WEATHER: String = "http://api.openweathermap.org/data/2.5/"

    private val gson: Gson = GsonBuilder()
        .setLenient()
        .create()

    private val client = OkHttpClient.Builder()

    private val retrofit: Retrofit.Builder = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))

    private fun setDefaultInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        return interceptor
    }

    // Create Service Basic Auth
    fun createService(username: String, password: String): ApiInterface {
        client.addInterceptor(setDefaultInterceptor())
        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
            val customInterceptor = object : Interceptor {
                override fun intercept(chain: Interceptor.Chain): Response {
                    val reqOri = chain.request()
                    val reqBuilder = reqOri.newBuilder().header("Authorization", Credentials.basic(username, password))
                    return chain.proceed(reqBuilder.build())
                }
            }
            client.addInterceptor(customInterceptor)
        }
        return createService(client.build())
    }

    // Create Service Using Custom API Key
    fun createService(apiKey: String?): ApiInterface {
        client.addInterceptor(setDefaultInterceptor())
        if (!TextUtils.isEmpty(apiKey)) {
            val customInterceptor = object : Interceptor {
                override fun intercept(chain: Interceptor.Chain): Response {
                    val reqOri = chain.request()
                    val reqBuilder = reqOri.newBuilder()
                        .header("Authorization", apiKey!!)
                        .header("Content-Type", "application/json")
                        .header("Accept", "*/*")
                        .method(reqOri.method, reqOri.body)
                    return chain.proceed(reqBuilder.build())
                }
            }
            client.addInterceptor(customInterceptor)
        }
        return createService(client.build())
    }

    // Create Service
    private fun createService(clientBuilder: OkHttpClient): ApiInterface =
        retrofit.client(clientBuilder).build().create(ApiInterface::class.java)

    // Client Client
    val getClient: ApiInterface
        get() {
            val gson = GsonBuilder()
                .setLenient()
                .create()
            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

            return retrofit.create(ApiInterface::class.java)
        }

    val getClientWeather: ApiInterface
        get() {
            val gson = GsonBuilder()
                .setLenient()
                .create()
            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL_WEATHER)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

            return retrofit.create(ApiInterface::class.java)
        }
}
package com.developer.aitek.earthquake.activities.fragments

import android.app.Activity
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.developer.aitek.earthquake.R
import com.developer.aitek.earthquake.activities.BmkgActivity
import com.developer.aitek.earthquake.models.*
import com.developer.aitek.earthquake.utilities.*
import com.microsoft.maps.*
import kotlinx.android.synthetic.main.component_bmkg.*
import kotlinx.android.synthetic.main.component_last_device.*
import kotlinx.android.synthetic.main.component_weather.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.item_bmkg.view.*
import kotlinx.android.synthetic.main.item_last_device.view.*
import kotlinx.android.synthetic.main.item_weather.view.*
import java.util.*


class HomeFragment : Fragment() {
    private lateinit var rootActivity: Activity
    private lateinit var address: Address
    private var bundle: Bundle? = null
    private lateinit var cacheUtil: CacheUtil

    // adapter
    private lateinit var deviceAdapter: AdapterUtil<LastDeviceData.Data>
    private lateinit var weatherAdapter: AdapterUtil<ItemWeather>
    private lateinit var bmkgAdapter: AdapterUtil<BMKG.Data.ItemData>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(rootActivity, getString(R.string.package_name))

        // Setup Weather Adapter
        weatherAdapter = AdapterUtil(R.layout.item_weather, listOf(),
            {itemView, item ->
                Glide.with(itemView)
                    .load(item.icon)
                    .into(itemView.item_weather_icon)
                itemView.item_weather_value.text = item.value
                itemView.item_weather_description.text = item.description
            },
            {_, _ ->})

        // Setup realtime last adapter
        deviceAdapter = AdapterUtil(R.layout.item_last_device, listOf(),
                {itemView, item ->
                    itemView.tv_tanggal.text = item.created_at
                    itemView.tv_pga.text = StringBuilder().append("PGA : ").append(item.pga)
                    itemView.tv_device.text = StringBuilder().append("Device : ").append(item.device_name)
                }, {_, _ -> })

        // Setup BMKG Adapter
        bmkgAdapter = AdapterUtil(R.layout.item_bmkg, listOf(),
            {itemView, item ->
                val mapView = MapView(requireContext(), MapRenderMode.VECTOR)
                mapView.setCredentialsKey("Aok9ufmaux1Tu0FUxlyBlrDheGp2nT2PwCMft9cOd2yjJWrWYBnivQq1mp_plzHP")
                itemView.item_bmkg_map.addView(mapView)
                mapView.onCreate(bundle)

                val addresses: List<Address>
                val geoCoder = Geocoder(requireContext(), Locale.getDefault())
                addresses = geoCoder.getFromLocation(
                    item.lat,
                    item.lon,
                    1
                )
                val itemAddress = addresses[0]

                val mPinLayer = MapElementLayer()
                mapView.layers.add(mPinLayer)

                log(itemAddress.toString())

                val location = Geopoint(item.lat, item.lon)
                val title = itemAddress.getAddressLine(0)
                val pushPin = MapIcon()
                pushPin.location = location
                pushPin.title = title
                mPinLayer.elements.add(pushPin)

                mapView.setScene(MapScene.createFromLocationAndRadius(location, 10000.toDouble()),
                    MapAnimationKind.LINEAR)

                itemView.item_bmkg_value.text = title
                itemView.item_bmkg_description.text = StringBuilder().append("Kedalaman : ").append(item.deep)
                    .append("\n").append("Magnitude : ").append(item.sr)
            },
            {_, item ->
                selected_bmkg = item
                startActivity(Intent(requireContext(), BmkgActivity::class.java))
                requireActivity().finish()
            })

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        this.address = Geocoder(requireContext(), Locale.getDefault())
            .getFromLocation(latitude, longitude, 1)[0]

        // Setup Identity
        val auth = auth(cacheUtil)
        tv_time.text = getTimeRegards()
        tv_name.text = StringBuilder().append(auth.full_name)
        tv_location.text = address.getAddressLine(0)

        // Setup Weather
        list_weather.layoutManager = GridLayoutManager(requireContext(), 2)
        list_weather.adapter = weatherAdapter
        list_weather.isNestedScrollingEnabled = false

        // Setup realtime last
        list_last_device.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        list_last_device.adapter = deviceAdapter

        // Setup BMKG
        list_bmkg.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, true)
        list_bmkg.adapter = bmkgAdapter

        // Load Weather
        this.loadWeather()

        // Load realtime last
        this.loadLastDataDevices()

        // Load BMKG
        this.loadBMKG()
    }

    private fun loadWeather() {
        loader_weather.visibility = View.VISIBLE
        getWeather(latitude, longitude, api_key, {
            val data: MutableList<ItemWeather> = arrayListOf()
            if (it.cod == 200) {
                data.add(
                    ItemWeather(R.drawable.ic_temperature,
                    StringBuilder().append((it.main.temp - 273.15).toInt())
                        .append("°C").toString(),
                    "temperature")
                )
                data.add(
                    ItemWeather(R.drawable.ic_weather,
                    StringBuilder().append(it.clouds.all).append("%").toString(),
                    it.weather[0].description)
                )
                data.add(
                    ItemWeather(R.drawable.ic_speed,
                        StringBuilder().append(it.wind.speed).append("km/h").toString(),
                        "wind speed")
                )
                data.add(
                    ItemWeather(R.drawable.ic_uv,
                        StringBuilder().append(it.main.humidity).append("RH").toString(),
                        "humadity")
                )
                weatherAdapter.data = data
            } else Toast.makeText(requireContext(), it.toString(), Toast.LENGTH_LONG).show()
            loader_weather.visibility = View.GONE
        }, {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            loader_weather.visibility = View.GONE
        })
    }

    private fun loadBMKG() {
        loader_bmkg.visibility = View.VISIBLE
        getBMKG(getFirstDayThisMonth(), getToday(), auth(this.cacheUtil).token, {
            if (it.code == 200) {
                val data: MutableList<BMKG.Data.ItemData> = arrayListOf()
                it.data.data.forEachIndexed { position, item ->
                    if (position < 6) {
                        data.add(item)
                    }
                }
                if (data.size > 0) bmkgAdapter.data = data
            } else Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()

            loader_bmkg.visibility = View.GONE
        }, {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            loader_bmkg.visibility = View.GONE
        })
    }

    private fun loadLastDataDevices() {
        loader_last_device.visibility = View.VISIBLE
        list_last_device.visibility = View.GONE

        getLastDataDevices(auth(this.cacheUtil).token, {
            if (it.code == 200)
                deviceAdapter.data = it.data
            else
                Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()

            loader_last_device.visibility = View.GONE
            list_last_device.visibility = View.VISIBLE
        }, {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            loader_last_device.visibility = View.GONE
            list_last_device.visibility = View.VISIBLE
        })
    }

    companion object {
        @JvmStatic
        fun newInstance(activity: Activity, bundle: Bundle?) =
            HomeFragment().apply {
                this.rootActivity = activity
                this.bundle = bundle
            }
    }
}
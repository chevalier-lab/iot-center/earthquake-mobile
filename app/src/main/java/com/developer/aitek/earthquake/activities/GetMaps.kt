package com.developer.aitek.earthquake.activities

import android.content.Intent
import android.location.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import com.developer.aitek.earthquake.R
import com.developer.aitek.earthquake.utilities.latitude
import com.developer.aitek.earthquake.utilities.log
import com.developer.aitek.earthquake.utilities.longitude
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.trontransportation.trontravel.utilities.LocationHelper
import java.util.*

class GetMaps : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, ActivityCompat.OnRequestPermissionsResultCallback {
    private var mLastLocation: Location? = null
    private var locationHelper: LocationHelper? = null
    private var locationManager : LocationManager? = null

    //define the listener
    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            val addresses: List<Address>
            val geoCoder = Geocoder(this@GetMaps, Locale.getDefault())

            addresses = geoCoder.getFromLocation(
                location.latitude,
                location.longitude,
                1
            )

            latitude = location.latitude
            longitude = location.longitude
        }
        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_maps)

        // Setup Location Helper
        locationHelper = LocationHelper(this@GetMaps)
        locationHelper!!.checkpermission()

        // check availability of play services
        if (locationHelper!!.checkPlayServices()) {

            // Building the GoogleApi client
            locationHelper!!.buildGoogleApiClient()

            mLastLocation = locationHelper?.location

            if (mLastLocation != null) {
                latitude = mLastLocation!!.latitude
                longitude = mLastLocation!!.longitude

                startActivity(Intent(this@GetMaps, DashboardActivity::class.java));
                finish()
            }
        }

        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?
        try {
            // Request location updates
            locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L,
                0f, locationListener)
        } catch(ex: SecurityException) {
            log(ex.stackTrace.toString())
        }
    }

    override fun onConnected(p0: Bundle?) {
        mLastLocation = locationHelper?.location

        startActivity(Intent(this@GetMaps, DashboardActivity::class.java));
        finish()
    }

    override fun onConnectionSuspended(p0: Int) {
        locationHelper?.connectApiClient()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        log(p0.errorCode.toString())
    }
}
package com.developer.aitek.earthquake.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.developer.aitek.earthquake.R
import com.developer.aitek.earthquake.utilities.CacheUtil
import com.developer.aitek.earthquake.utilities.token

class CheckIsAlreadyAuthActivity : AppCompatActivity() {

    private lateinit var cacheUtil: CacheUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_is_already_auth)

        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(this@CheckIsAlreadyAuthActivity, getString(R.string.package_name))

        if (this.cacheUtil.get(token) != null) {
            startActivity(Intent(this@CheckIsAlreadyAuthActivity,
                GetMaps::class.java))
        } else {
            startActivity(Intent(this@CheckIsAlreadyAuthActivity,
                LoginActivity::class.java))
        }
        finish()
    }
}
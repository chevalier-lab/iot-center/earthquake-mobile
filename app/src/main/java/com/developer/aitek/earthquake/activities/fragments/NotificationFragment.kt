package com.developer.aitek.earthquake.activities.fragments

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.developer.aitek.earthquake.R
import com.developer.aitek.earthquake.models.Notification
import com.developer.aitek.earthquake.models.getNotifications
import com.developer.aitek.earthquake.utilities.AdapterUtil
import com.developer.aitek.earthquake.utilities.CacheUtil
import com.developer.aitek.earthquake.utilities.auth
import com.developer.aitek.earthquake.utilities.token
import kotlinx.android.synthetic.main.component_notification.*
import kotlinx.android.synthetic.main.item_notification.view.*

class NotificationFragment : Fragment() {
    private lateinit var rootActivity: Activity
    private var bundle: Bundle? = null
    private lateinit var notificationAdapter: AdapterUtil<Notification.Data>
    private lateinit var cacheUtil: CacheUtil

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(rootActivity, getString(R.string.package_name))

        // Setup Notification Adapter
        notificationAdapter = AdapterUtil(R.layout.item_notification, listOf(),
            { itemView, item ->
                itemView.item_notification_pga.text = StringBuilder()
                    .append("PGA: ").append(item.pga)
                itemView.item_notification_device_name.text = item.device.device_name
                itemView.item_notification_date_time.text = item.created_at
            },
            { _, _ -> })

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Setup Notification
        list_notification.layoutManager = LinearLayoutManager(requireContext())
        list_notification.adapter = notificationAdapter
        list_notification.isNestedScrollingEnabled = false

        // Load Notification
        this.loadNotification()
    }

    private fun loadNotification() {
        loader_notification.visibility = View.VISIBLE
        list_notification.visibility = View.GONE
        getNotifications(auth(this.cacheUtil).token, {
            if (it.code == 200) {
                notificationAdapter.data = it.data
            } else Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()

            loader_notification.visibility = View.GONE
            list_notification.visibility = View.VISIBLE
        }, {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            loader_notification.visibility = View.GONE
            list_notification.visibility = View.VISIBLE
        })
    }

    companion object {
        @JvmStatic
        fun newInstance(activity: Activity, bundle: Bundle?) =
            NotificationFragment().apply {
                this.rootActivity = activity
                this.bundle = bundle
            }
    }
}
package com.developer.aitek.earthquake.activities

import android.content.Intent
import android.location.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.text.TextUtils
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.developer.aitek.earthquake.R
import com.developer.aitek.earthquake.models.doLogin
import com.developer.aitek.earthquake.utilities.*
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.gson.JsonObject
import com.trontransportation.trontravel.utilities.LocationHelper
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.loader
import java.util.*

class LoginActivity : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, ActivityCompat.OnRequestPermissionsResultCallback {
    private var mLastLocation: Location? = null
    private var locationHelper: LocationHelper? = null
    private var locationManager : LocationManager? = null

    //define the listener
    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            val addresses: List<Address>
            val geoCoder = Geocoder(this@LoginActivity, Locale.getDefault())

            addresses = geoCoder.getFromLocation(
                location.latitude,
                location.longitude,
                1
            )

            latitude = location.latitude
            longitude = location.longitude
        }
        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

    private lateinit var cacheUtil: CacheUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        // Setup Location Helper
        locationHelper = LocationHelper(this@LoginActivity)
        locationHelper!!.checkpermission()

        // check availability of play services
        if (locationHelper!!.checkPlayServices()) {

            // Building the GoogleApi client
            locationHelper!!.buildGoogleApiClient()

            mLastLocation = locationHelper?.location

            if (mLastLocation != null) {
                latitude = mLastLocation!!.latitude
                longitude = mLastLocation!!.longitude
            }
        }

        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?
        try {
            // Request location updates
            locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L,
                0f, locationListener)
        } catch(ex: SecurityException) {
            log(ex.stackTrace.toString())
        }

        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(this@LoginActivity, getString(R.string.package_name))

        tv_sign_up.text = Html.fromHtml(getString(R.string.message_sign_up))

        // Show Hide Password
        var isShow = false
        edt_password.setOnTouchListener { view, event ->
            val DRAWABLE_RIGHT = 2;

            if(event.action == MotionEvent.ACTION_UP) {
                if(event.rawX >=
                    (edt_password.right - edt_password.compoundDrawables[DRAWABLE_RIGHT].bounds.width())) {
                    // your action here
                    if (isShow) {
                        isShow = false
                        edt_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_close, 0)
                        edt_password.transformationMethod = PasswordTransformationMethod.getInstance()
                    } else {
                        isShow = true
                        edt_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eye_open, 0)
                        edt_password.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    }

                    return@setOnTouchListener true
                }
            }
            return@setOnTouchListener false
        }

        // Handler Inputs
        setActivationButton()
        edt_username.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                setActivationButton()
            }
        })
        edt_password.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                setActivationButton()
            }
        })

        // Handler Sign In
        btn_sign_in.setOnClickListener {
            loader.visibility = View.VISIBLE
            btn_sign_in.visibility = View.GONE
            val data = JsonObject()
            data.addProperty("username", edt_username.text.toString())
            data.addProperty("password", edt_password.text.toString())
            doLogin(data, {
                if (it.code == 200) {
                    this.cacheUtil.set(token, it.data)
                    startActivity(Intent(this@LoginActivity, CheckIsAlreadyAuthActivity::class.java))
                    finish()
                } else Toast.makeText(applicationContext, it.message, Toast.LENGTH_LONG).show()
            }, {
                Toast.makeText(applicationContext, it, Toast.LENGTH_LONG).show()
            })
        }

        // Handler Sign Up
        tv_sign_up.setOnClickListener {
            startActivity(Intent(this@LoginActivity, RegistrationActivity::class.java))
            finish()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        // redirects to utils
        locationHelper!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun setActivationButton() {
        if (checkIsFilled()) {
            btn_sign_in.isEnabled = true
            btn_sign_in.setBackgroundResource(R.drawable.background_round_orange)
            btn_sign_in.setTextColor(
                ContextCompat.getColor(applicationContext,
                R.color.colorWhite))
        } else {
            btn_sign_in.isEnabled = false
            btn_sign_in.setBackgroundResource(R.drawable.background_round_dark_gray)
            btn_sign_in.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorBlack))
        }
    }

    private fun checkIsFilled(): Boolean {
        return !(TextUtils.isEmpty(edt_username.text.toString()) ||
                TextUtils.isEmpty(edt_password.text.toString()))
    }

    override fun onConnected(p0: Bundle?) {
        mLastLocation = locationHelper?.location
    }

    override fun onConnectionSuspended(p0: Int) {
        locationHelper?.connectApiClient()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        log(p0.errorCode.toString())
    }
}
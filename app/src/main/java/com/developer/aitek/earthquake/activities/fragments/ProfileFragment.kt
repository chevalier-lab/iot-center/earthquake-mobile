package com.developer.aitek.earthquake.activities.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.developer.aitek.earthquake.R
import com.developer.aitek.earthquake.activities.LoginActivity
import com.developer.aitek.earthquake.models.getProfile
import com.developer.aitek.earthquake.utilities.AdapterUtil
import com.developer.aitek.earthquake.utilities.CacheUtil
import com.developer.aitek.earthquake.utilities.auth
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.item_profile.view.*

class ProfileFragment : Fragment() {
    private lateinit var rootActivity: Activity
    private var bundle: Bundle? = null
    private lateinit var cacheUtil: CacheUtil
    private lateinit var profileAdapter: AdapterUtil<String>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(rootActivity, getString(R.string.package_name))

        // Setup profile adapter
        this.profileAdapter = AdapterUtil(R.layout.item_profile, listOf(
            "Ubah Profil",
            "Kebijakan dan Privasi",
            "Keluar"
        ),
            { itemView, item ->
                itemView.item_profile_label.text = item
            },
            { position, _ ->
                if (position == 2) {
                    this.cacheUtil.clear()
                    this.cacheUtil.destroy()
                    startActivity(Intent(requireContext(), LoginActivity::class.java))
                    requireActivity().finish()
                }
            })

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Setup Profile Menu
        list_profile.layoutManager = LinearLayoutManager(requireContext())
        list_profile.adapter = profileAdapter

        this.loadProfile()
    }

    private fun loadProfile() {
        loader_profile.visibility = View.VISIBLE
        list_profile.visibility = View.GONE
        getProfile(auth(this.cacheUtil).token, {
            if (it.code == 200) {
                // Setup Profile
                Glide.with(requireContext())
                    .load(it.data.utility.face.uri)
                    .circleCrop()
                    .into(img_face)
                tv_name.text = it.data.full_name
            } else Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()

            loader_profile.visibility = View.GONE
            list_profile.visibility = View.VISIBLE
        }, {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()

            loader_profile.visibility = View.GONE
            list_profile.visibility = View.VISIBLE
        })
    }

    companion object {
        @JvmStatic
        fun newInstance(activity: Activity, bundle: Bundle?) =
            ProfileFragment().apply {
                this.rootActivity = activity
                this.bundle = bundle
            }
    }
}
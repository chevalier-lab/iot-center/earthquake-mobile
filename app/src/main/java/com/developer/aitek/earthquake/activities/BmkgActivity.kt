package com.developer.aitek.earthquake.activities

import android.content.Intent
import android.location.Address
import android.location.Geocoder
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.developer.aitek.earthquake.R
import com.developer.aitek.earthquake.models.ItemWeather
import com.developer.aitek.earthquake.utilities.AdapterUtil
import com.developer.aitek.earthquake.utilities.log
import com.developer.aitek.earthquake.utilities.selected_bmkg
import com.microsoft.maps.*
import kotlinx.android.synthetic.main.activity_bmkg.*
import kotlinx.android.synthetic.main.item_weather.view.*
import java.util.*

class BmkgActivity : AppCompatActivity() {

    private lateinit var bmkgAdapter: AdapterUtil<ItemWeather>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bmkg)

        selected_bmkg?.let {
            val mapView = MapView(this@BmkgActivity, MapRenderMode.VECTOR)
            mapView.setCredentialsKey("Aok9ufmaux1Tu0FUxlyBlrDheGp2nT2PwCMft9cOd2yjJWrWYBnivQq1mp_plzHP")
            map.addView(mapView)
            mapView.onCreate(savedInstanceState)

            val addresses: List<Address>
            val geoCoder = Geocoder(this@BmkgActivity, Locale.getDefault())
            addresses = geoCoder.getFromLocation(
                it.lat,
                it.lon,
            1
            )
            val itemAddress = addresses[0]

            val mPinLayer = MapElementLayer()
            mapView.layers.add(mPinLayer)

            log(itemAddress.toString())

            val location = Geopoint(it.lat, it.lon)
            val title = itemAddress.getAddressLine(0)
            val pushPin = MapIcon()
            pushPin.location = location
            pushPin.title = title
            mPinLayer.elements.add(pushPin)

            mapView.setScene(
                MapScene.createFromLocationAndRadius(location, 30000.toDouble()),
                MapAnimationKind.LINEAR)

            bmkgAdapter = AdapterUtil(R.layout.item_weather, listOf(
                ItemWeather(R.drawable.ic_time, "Date", selected_bmkg!!.timestamp),
                ItemWeather(R.drawable.ic_magnitude, "Magnitude", StringBuilder().append(selected_bmkg!!.sr)
                    .append(" SR").toString()),
                ItemWeather(R.drawable.ic_time, "Deep", StringBuilder().append(selected_bmkg!!.deep)
                    .append(" KM").toString())
            ),
                { itemView, item ->
                    Glide.with(itemView)
                        .load(item.icon)
                        .into(itemView.item_weather_icon)
                    itemView.item_weather_value.text = item.value
                    itemView.item_weather_description.text = item.description
                },
                { _, _ -> })
            list_bmkg_detail.layoutManager = GridLayoutManager(this@BmkgActivity, 3)
            list_bmkg_detail.adapter = bmkgAdapter
            tv_location.text = StringBuilder().append("Location: ").append(title)
        }
    }

    override fun onBackPressed() {
        startActivity(Intent(this@BmkgActivity, DashboardActivity::class.java))
        finish()
    }
}
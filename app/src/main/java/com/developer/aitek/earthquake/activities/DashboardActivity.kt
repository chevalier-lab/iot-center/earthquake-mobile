package com.developer.aitek.earthquake.activities

import android.location.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.developer.aitek.earthquake.R
import com.developer.aitek.earthquake.activities.fragments.DeviceFragment
import com.developer.aitek.earthquake.activities.fragments.HomeFragment
import com.developer.aitek.earthquake.activities.fragments.NotificationFragment
import com.developer.aitek.earthquake.activities.fragments.ProfileFragment
import com.developer.aitek.earthquake.utilities.current_dashboard
import com.developer.aitek.earthquake.utilities.latitude
import com.developer.aitek.earthquake.utilities.log
import com.developer.aitek.earthquake.utilities.longitude
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.trontransportation.trontravel.utilities.LocationHelper
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.component_bottom_bar.*
import java.util.*

class DashboardActivity : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, ActivityCompat.OnRequestPermissionsResultCallback {

    private var mLastLocation: Location? = null
    private var locationHelper: LocationHelper? = null
    private var locationManager : LocationManager? = null
    private var isAlreadyLoad = false
    private var savedInstanceState: Bundle? = null

    //define the listener
    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            val addresses: List<Address>
            val geoCoder = Geocoder(this@DashboardActivity, Locale.getDefault())

            addresses = geoCoder.getFromLocation(
                location.latitude,
                location.longitude,
                1
            )

            latitude = location.latitude
            longitude = location.longitude

            loader.visibility = View.GONE

            getAddress()
        }
        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_dashboard)

        this.savedInstanceState = savedInstanceState

        // Setup Loader
        loader.visibility = View.VISIBLE

        // Setup Location Helper
        locationHelper = LocationHelper(this@DashboardActivity)
        locationHelper!!.checkpermission()

        bottom_bar.setOnNavigationItemSelectedListener(this.navigationItemSelectedListener)

        // check availability of play services
        if (locationHelper!!.checkPlayServices()) {

            // Building the GoogleApi client
            locationHelper!!.buildGoogleApiClient()

            mLastLocation = locationHelper?.location

            if (mLastLocation != null) {
                latitude = mLastLocation!!.latitude
                longitude = mLastLocation!!.longitude
                getAddress()
            }
        }

        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?
        try {
            // Request location updates
            locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L,
                0f, locationListener)
        } catch(ex: SecurityException) {
            log(ex.stackTrace.toString())
        }
    }

    private fun getAddress() {
        val locationAddress: Address? = locationHelper!!.getAddress(latitude, longitude)
        if (locationAddress != null) {
            if (!isAlreadyLoad) {
                isAlreadyLoad = true
                when(current_dashboard) {
                    0 -> {
                        bottom_bar.selectedItemId = R.id.item_home
//                        this.setFragment(HomeFragment.newInstance(this@DashboardActivity, this.savedInstanceState))
                    }
                    1 -> {
                        bottom_bar.selectedItemId = R.id.item_device
//                        this.setFragment(DeviceFragment.newInstance(this@DashboardActivity, this.savedInstanceState))
                    }
                    2 -> {
                        bottom_bar.selectedItemId = R.id.item_notification
//                        this.setFragment(NotificationFragment.newInstance(this@DashboardActivity, this.savedInstanceState))
                    }
                    else -> {
                        bottom_bar.selectedItemId = R.id.item_profile
//                        this.setFragment(ProfileFragment.newInstance(this@DashboardActivity, this.savedInstanceState))
                    }
                }
            }
        } else Toast.makeText(this@DashboardActivity, "Something went wrong", Toast.LENGTH_SHORT).show()
        loader.visibility = View.GONE
    }

    private val navigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener {
        when (it.itemId) {
            R.id.item_home -> {
                current_dashboard = 0
                this.setFragment(HomeFragment.newInstance(this@DashboardActivity, this.savedInstanceState))
            }
            R.id.item_device -> {
                current_dashboard = 1
                this.setFragment(DeviceFragment.newInstance(this@DashboardActivity, this.savedInstanceState))
            }
            R.id.item_notification -> {
                current_dashboard = 2
                this.setFragment(NotificationFragment.newInstance(this@DashboardActivity, this.savedInstanceState))
            }
            R.id.item_profile -> {
                current_dashboard = 3
                this.setFragment(ProfileFragment.newInstance(this@DashboardActivity, this.savedInstanceState))
            }
        }
        return@OnNavigationItemSelectedListener true
    }

    private fun setFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.frame_dashboard, fragment).commit()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        // redirects to utils
        locationHelper!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onConnected(p0: Bundle?) {
        mLastLocation = locationHelper?.location
    }

    override fun onConnectionSuspended(p0: Int) {
        locationHelper?.connectApiClient()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        log(p0.errorCode.toString())
    }
}
package com.developer.aitek.earthquake.models

// Login
data class Login(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: Data
) {
    data class Data(
        var full_name: String,
        var username: String,
        var password: String,
        var phone_number: String,
        var level: Int,
        var token: String
    )
}

// Profile
data class Profile(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: Data
) {
    data class Data(
        var full_name: String,
        var username: String,
        var password: String,
        var phone_number: String,
        var utility: Utility
    ) {
        data class Utility(
            var level: Int,
            var token: String,
            var face: Face
        ) {
            data class Face(
                var uri: String,
                var label: String
            )
        }
    }
}

// Data Device
data class DataDevice(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: MutableList<Data>
) {
    data class Data(
        var id: Long,
        var id_m_devices: Long,
        var pga: Double,
        var date: String,
        var time: String,
        var created_at: String,
        var updated_at: String
    )
}


// Weather
data class Weather(
    var id: Long,
    var name: String,
    var cod: Int,
    var timezone: Long,
    var dt: Long,
    var visibility: Long,
    var base: String,
    var coord: Coordinate,
    var weather: MutableList<WeatherItem>,
    var main: Main,
    var wind: Wind,
    var clouds: Cloud,
    var sys: Sys
) {
    data class Coordinate(
        var lat: Double,
        var lon: Double
    )

    data class WeatherItem(
        var id: Long,
        var main: String,
        var description: String,
        var icon: String
    )

    data class Main(
        var temp: Double,
        var feels_like: Double,
        var temp_min: Double,
        var temp_max: Double,
        var pressure: Long,
        var humidity: Int,
        var sea_level: Long,
        var grnd_level: Long
    )

    data class Wind(
        var speed: Double,
        var deg: Int
    )

    data class Cloud(
        var all: Int
    )

    data class Sys(
        var country: String,
        var sunrise: Long,
        var sunset: Long
    )
}

// BMKG
data class BMKG(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: Data
) {
    data class Data(
        var recordsTotal: Int,
        var data: MutableList<ItemData> = arrayListOf()
    ) {
        data class ItemData(
            var timestamp: String,
            var lat: Double,
            var lon: Double,
            var deep: Double,
            var sr: Double
        )
    }
}

// Devices
data class Device(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: MutableList<Data>
) {
    data class Data(
        var id: Long,
        var id_m_maps: Long,
        var type: Int,
        var device_name: String,
        var short_name: String,
        var device_token: String,
        var created_at: String,
        var updated_at: String,
        var alarm_status: String,
        var map: Map? = null
    ) {
        data class Map(
            var lat: Double,
            var lon: Double,
            var content: String,
            var description: String,
            var caption: String,
            var preset: String
        )
    }
}

// Notification
data class Notification(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: MutableList<Data>
) {
    data class Data(
        var id: Long,
        var id_m_devices: Long,
        var pga: Double,
        var date: String,
        var time: String,
        var created_at: String,
        var updated_at: String,
        var device: Device
    ) {
        data class Device(
            var id: Long,
            var id_m_maps: Long,
            var type: Int,
            var device_name: String,
            var short_name: String,
            var device_token: String,
            var created_at: String,
            var updated_at: String,
            var map: Map? = null
        ) {
            data class Map(
                var lat: Double,
                var lon: Double,
                var content: String,
                var description: String,
                var caption: String,
                var preset: String
            )
        }
    }
}

// Data Device
data class LastDeviceData(
        var code: Int,
        var error: MutableList<String> = arrayListOf(),
        var message: String = "",
        var data: MutableList<Data>
) {
    data class Data(
            var id: Long,
            var id_m_devices: Long,
            var pga: Double,
            var date: String,
            var time: String,
            var created_at: String,
            var updated_at: String,
            var device_name: String,
            var short_name: String
    )
}

// Switch Alarm Device
data class SwitchAlarmDevice(
    var code: Int,
    var error: MutableList<String> = arrayListOf(),
    var message: String = "",
    var data: Data
) {
    data class Data(
        var status: String
    )
}
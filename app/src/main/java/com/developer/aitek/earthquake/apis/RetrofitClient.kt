package com.developer.aitek.earthquake.apis

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

import java.util.concurrent.TimeUnit

object RetrofitClient {
    private var retrofit: Retrofit? = null
    private var retrofitt: Retrofit? = null
    var UserKey = ""

    fun getClient(baseUrl: String): Retrofit? {
        val builder = OkHttpClient().newBuilder()
        builder.readTimeout(10, TimeUnit.SECONDS)
        builder.connectTimeout(5, TimeUnit.SECONDS)

        if (retrofit == null) {
            builder.addInterceptor { chain ->
                val request = chain.request().newBuilder().addHeader("session", UserKey).build()
                chain.proceed(request)
            }

            val client = builder.build()

            retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofit
    }

    fun getClientt(baseUrl: String): Retrofit? {
        val builder = OkHttpClient().newBuilder()
        builder.readTimeout(10, TimeUnit.SECONDS)
        builder.connectTimeout(5, TimeUnit.SECONDS)

        if (retrofitt == null) {
            builder.addInterceptor { chain ->
                val request = chain.request().newBuilder().addHeader("session", UserKey).build()
                chain.proceed(request)
            }

            val client = builder.build()

            retrofitt = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofitt
    }
}

package com.developer.aitek.earthquake.utilities

import android.util.Log
import com.developer.aitek.earthquake.models.BMKG
import com.developer.aitek.earthquake.models.Device
import com.developer.aitek.earthquake.models.Login
import com.google.gson.Gson
import java.text.SimpleDateFormat
import java.util.*

const val tag = "EARTHQUAKE_APP"
const val api_key = "bfd31441d591dc093d6ac0027c2c2754"
const val token = "EARTHQUAKE_APP_TOKEN"
//const val token = "be171fb2f64d1cf4653490620f468b1d"

var latitude = 0.0
var longitude = 0.0

var current_dashboard = 0
var selected_device: Device.Data? = null
var selected_bmkg: BMKG.Data.ItemData? = null

fun convertDateToIDFormat(date: Date): String {
    val formatter = SimpleDateFormat("EEEE, dd MMM yyyy", Locale("ID"))
    return formatter.format(date)
}

fun getTimeRegards(): String {
    val calendar = Calendar.getInstance()
    val hour = calendar.get(Calendar.HOUR_OF_DAY)
    log(hour.toString())
    return when {
        hour < 9 -> "Selamat pagi"
        hour < 14 -> "Selamat siang"
        hour < 18 -> "Selamat sore"
        else -> "Selamat malam"
    }
}

fun getFirstDayThisMonth(): String {
    val calendar = Calendar.getInstance()
    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH))
    val formatter = SimpleDateFormat("yyyy-MM-dd", Locale("ID"))
    return formatter.format(calendar.time)
}

fun getToday(): String {
    val formatter = SimpleDateFormat("yyyy-MM-dd", Locale("ID"))
    return formatter.format(Date())
}

fun auth(cacheUtil: CacheUtil): Login.Data =
    Gson().fromJson(cacheUtil.get(token), Login.Data::class.java)

fun log(msg: String) { Log.d(tag, msg) }
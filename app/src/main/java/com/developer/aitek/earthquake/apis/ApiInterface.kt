package com.developer.aitek.earthquake.apis

import com.developer.aitek.earthquake.models.*
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ApiInterface {
    // General ==================================================================

    // Login
    @POST("user/login")
    fun login(@Body json: JsonObject): Call<Login>

    // Registration
    @POST("user/registration")
    fun registration(@Body json: JsonObject): Call<Login>

    // Profile
    @GET("user/profile")
    fun profile(): Call<Profile>

    // Data Device
    @GET("dataDevice/get_montly")
    fun dataDevice(): Call<DataDevice>

    // BMKG
    @GET("bMKG/get_data_today")
    fun getThisMonthBMKG(
        @Query("date_from") date_from: String,
        @Query("date_to") date_to: String
    ): Call<BMKG>

    // Devices
    @GET("devices/get_devices")
    fun getDevices(): Call<Device>

    // Notification
    @GET("deviceNotification/get_montly")
    fun getNotification(): Call<Notification>

    // Weather ==================================================================
    @GET("weather")
    fun getWeather(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("appid") api_key: String
    ): Call<Weather>

    // devices data
    @GET("dataDevice/get_last")
    fun getLastDataDevice(): Call<LastDeviceData>

    // Alarm
    @POST("alarm/swithAlarm")
    fun switchAlarmDevice(
        @Query("id_device") id_device: Long,
        @Body json: JsonObject
    ): Call<SwitchAlarmDevice>
}


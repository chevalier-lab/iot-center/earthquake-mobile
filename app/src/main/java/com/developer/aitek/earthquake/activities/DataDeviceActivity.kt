package com.developer.aitek.earthquake.activities

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.developer.aitek.earthquake.R
import com.developer.aitek.earthquake.models.DataDevice
import com.developer.aitek.earthquake.models.getDataDevice
import com.developer.aitek.earthquake.utilities.AdapterUtil
import com.developer.aitek.earthquake.utilities.CacheUtil
import com.developer.aitek.earthquake.utilities.auth
import com.developer.aitek.earthquake.utilities.selected_device
import kotlinx.android.synthetic.main.activity_data_device.*
import kotlinx.android.synthetic.main.item_data_device.view.*

class DataDeviceActivity : AppCompatActivity() {

    private lateinit var cacheUtil: CacheUtil
    private lateinit var dataDeviceAdapter: AdapterUtil<DataDevice.Data>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_device)

        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(this@DataDeviceActivity, getString(R.string.package_name))

        // Setup Toolbar
        setupToolbar()

        // Setup data device
        dataDeviceAdapter = AdapterUtil(R.layout.item_data_device, listOf(),
            { itemView, item ->
                itemView.item_data_device_pga.text = StringBuilder()
                    .append("PGA: ").append(item.pga)
                itemView.item_data_device_device_name.text = StringBuilder().append(selected_device!!.device_name)
                itemView.item_data_device_date_time.text = StringBuilder().append(item.created_at)
            },
            { _, _ -> })

        list_data_device.layoutManager = LinearLayoutManager(this@DataDeviceActivity)
        list_data_device.adapter = dataDeviceAdapter

        // Load Data Device All
        this.loadDataDevice()
    }

    private fun loadDataDevice() {
        selected_device?.let {
            loader_data_device.visibility = View.VISIBLE
            list_data_device.visibility = View.GONE
            getDataDevice(auth(this.cacheUtil).token, {
                if (it.code == 200) {
                    dataDeviceAdapter.data = it.data
                } else Toast.makeText(this@DataDeviceActivity, it.message, Toast.LENGTH_LONG).show()

                loader_data_device.visibility = View.GONE
                list_data_device.visibility = View.VISIBLE
            }, {
                Toast.makeText(this@DataDeviceActivity, it, Toast.LENGTH_LONG).show()

                loader_data_device.visibility = View.GONE
                list_data_device.visibility = View.VISIBLE
            })
        }
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        if (selected_device != null) supportActionBar!!.title = "Data ${selected_device!!.device_name}"
        else supportActionBar!!.title = "Data Device"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            this.onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        startActivity(Intent(this@DataDeviceActivity, DashboardActivity::class.java))
        finish()
    }
}